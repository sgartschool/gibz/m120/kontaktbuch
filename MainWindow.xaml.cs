﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kontaktbuch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //List<Contact> contacts = new List<Contact>();
        Contact contact;

        public MainWindow()
        {
            InitializeComponent();
            contact = new Contact("Max", "Mustermann", new Adress("6314", "Unteraegeri"));
            this.DataContext = contact;
        }

        //private void CreateContactFromInput(object sender, RoutedEventArgs e)
        //{
        //    string firstName = inputFirstName.Text;
        //    string lastName = inputLastName.Text;
        //    string postcode = inputPostleitzahl.Text;
        //    string location = inputOrt.Text;

        //    contacts.Add(new Contact(firstName, lastName, postcode, location));

        //    inputFirstName.Text = "";
        //    inputLastName.Text = "";
        //    inputPostleitzahl.Text = "";
        //    inputOrt.Text = "";
        //}


    }
}