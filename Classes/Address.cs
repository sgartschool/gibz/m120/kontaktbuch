﻿namespace Kontaktbuch
{
    class Adress : BaseModel
    {
        private string _postcode;
        public string postcode { get => _postcode; set { _postcode = value; OnPropertyChanged("postcode"); } }
        private string _location;
        public string location { get => _location; set { _location = value; OnPropertyChanged("location"); } }

        public Adress(string postcode, string location)
        {
            this.postcode = postcode;
            this.location = location;
        }
    }
}