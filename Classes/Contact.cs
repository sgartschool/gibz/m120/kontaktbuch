﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Kontaktbuch
{
    class Contact : BaseModel
    {
        public string firstName { get => _firstname; set { _firstname = value; OnPropertyChanged("firstName"); } }
        private string _firstname;
        public string lastName { get => _lastName; set { _lastName = value; OnPropertyChanged("lastName"); } }
        private string _lastName;
        public Adress adress { get => _adress; set { _adress = value; OnPropertyChanged("address"); } }
        private Adress _adress;

        public Contact(string firstName, string lastName, string postcode, string location)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.adress = new Adress(postcode, location);
        }
        public Contact(string firstName, string lastName, Adress adress)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.adress = adress;
        }
    }
}